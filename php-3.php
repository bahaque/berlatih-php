<?php

function tentukan_nilai($number)
{
    if ($number >= 85 && $number <= 100) {
    	echo "Sangat Baik";
    }
    else if ($number >= 70 && $number < 85) {
    	echo "Baik";
    }
    else if ($number >= 60 && $number < 70) {
    	echo "Cukup";
    }
    else {
    	echo "Kurang";
    }
    echo "<br/>";
}

//TEST CASES
echo "NO. 1 TENTUKAN NILAI <br/>";
echo "<br/>";

echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang

echo "<br/>";
echo "<br/>";


echo "NO. 2 UBAH HURUF <br/>";
echo "<br/>";

function ubah_huruf($string){
	$huruf = ['a', 'b', 'c', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
	$wadah = "";

	$length = strlen($string);

        for ($i = 0; $i < strlen($string); $i++) { 
            for ($j = 0; $j < count($huruf); $j++) { 
             	if ($huruf[$j] == $string[$i]) {
             		$huruf[$i].' => '.$huruf[$j + 1].'<br/>';
             		$wadah .= $huruf[$j + 1];
             	}
             } 
        }
        echo $wadah;
    	echo "<br/>";
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

//ATAU 

// function ubahHuruf($str) {
// 	$abjad = "abcdefghijklmnopqrstuvwxyz";
// 	$output = "";

// 	for ($i = 0; $i < strlen($str); $i++) { //tentukan panjang dr variabel stringnya
// 		$position = strrpos($abjad, $str[$i]); 
// 		// utk mengetahui letak dr string nya. 
// 		// fungsi strrpos : memberi indeks dr sebuah string
// 		$output .= substr($abjad, $position + 1, 1);
// 	}
// 	echo "<br/>";
// 	return $output;
// }
// echo ubahHuruf('wow');
// echo ubahHuruf('developer'); // efwfmpqfs
// echo ubahHuruf('laravel'); // mbsbwfm
// echo ubahHuruf('keren'); // lfsfo
// echo ubahHuruf('semangat'); // tfnbohbu

echo "<br/>";
echo "<br/>";

echo "NO. 3 TUKAR BESAR KECIL <br/>";
echo "<br/>";

function tukar_besar_kecil($string){
	$output = "";

	for ($i = 0; $i < strlen($string); $i++) { 
		if (ctype_lower($string[$i])) { // ctype_lower = utk ngecek apa string huruf kecil
			$output .= strtoupper($string[$i]);
		}
		else {
			$output .= strtolower($string[$i]);
		}
	}
	return $output . "<br/>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
?>