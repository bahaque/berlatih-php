<?php  
/*
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
*/
class Animal {
	// prperty = data yg menempel
	public $legs = 2; // property / variabel yg ad dlm object (member variabel). visibility + variabel => public/private/protected + $variabel
	public $cold_blooded = "false";
	public $name;


	// method = fungsi dlm object <==> prilaku
	public function __construct($name) {
		$this->name = $name;
	}

	public function get_name() {
		return "$this->name";
	}

	public function get_legs() {
		return "$this->legs";
	}

	public function get_cold_blooded() {
		return "$this->cold_blooded";
	}
}

class Frog extends Animal {
	public $jump = "hop hop";

	public function jump() {
		return "$this->jump";
	}
}

class Ape extends Animal {
	public $yell = "Auooo";

	public function yell() {
		return "$this->yell";
	}
}

?>

