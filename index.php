<?php  

require ('animal.php');

$sheep = new Animal("shaun"); // object = instance yg didefinisikan oleh Class. keyword object ==> new
echo "RELEASE 0";
echo "<br/>";
echo "<br/>";

echo $sheep->get_name(); // "shaun"
echo "<br/>";
echo $sheep->get_legs(); // 2
echo "<br/>";
echo $sheep->get_cold_blooded(); // false// 
echo "<br/>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<br/>";
echo "<br/>";

echo "RELEASE 1";
echo "<br/>";
echo "<br/>";
// index.php
$sungokong = new Ape("kera sakti");
echo $sungokong->yell(); // "Auooo"

echo "<br/>";

$kodok = new Frog("buduk");
$kodok->legs = 4; // menimpa property legs yg bernilai 2 ke nilai 4
echo $kodok->jump(); // "hop hop"

?>